{
  description = "Script for syncing Final Fantasy XIV configs";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        name = "final-fantasy-xiv-sync";
        buildInputs = with pkgs; [ git ];
        script = (pkgs.writeScriptBin name (builtins.readFile ./final-fantasy-xiv-sync)).overrideAttrs (old: {
          # This patch /bin/sh to /nix/store/<hash>-some-bash/bin/sh
          # see https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/setup-hooks/patch-shebangs.sh
          buildCommand = "${old.buildCommand}\n patchShebangs $out";
        });
        package = pkgs.symlinkJoin {
          inherit name;
          paths = [ script ] ++ buildInputs;
          buildInputs = [ pkgs.makeWrapper ];
          postBuild = "wrapProgram $out/bin/${name} --prefix PATH : $out/bin";
        };
      in
      rec {
        apps = { ${name} = { type = "app"; program = "${defaultPackage}/bin/${name}"; }; };
        defaultPackage = package;
        packages.${name} = defaultPackage;
        defaultApp = apps.${name};
      }
    );
}
